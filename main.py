import webapp

form = """
    <form action="" method="POST">
            <p>Introduce url: <input type="text" name="url"/>
                shortened URL: <input type="text" name="short"/>
                <input type="submit" value="Send url"/>
    </form>
"""


class Shortener(webapp.webApp):
    content: dict = {}

    def parse(self, request):
        # Returns the method of the request, the resource and the body of the request

        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\r\n\r\n', 1)[1]

        return method, resource, body

    def format_urls(self) -> str:

        info: str = ""
        for url in self.content:
            info = info + '<br>' + self.content[url] + ' <b>as</b> ' + url
        return info

    def process(self, parsedRequest):
        method, resource, body = parsedRequest

        if method == "GET":
            # if petition is in root page
            if resource == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body><p>" + form + "<p> shortened URLs:</p>" + \
                           "<p>" + self.format_urls() + "</p>" + \
                           "</body></html>"
            # if petition is in dictionary
            elif resource in self.content:
                httpCode = "302 Found"
                htmlBody = "<html><body><meta http-equiv='refresh' content='2;" + \
                           "url=" + self.content[resource] + "'></body></html>"
            # if petition is unknown
            else:
                httpCode = "404 not Found"
                htmlBody = "<html><body><p>Resource: " + resource + \
                           " unavailable,</body></html>"
            return httpCode, htmlBody

        if method == "POST":

            content = body.split('&')
            urlaux = content[0].split('=')[1]
            shortaux = content[1].split('=')[1]
            url = urlaux.replace('%3A', ':').replace('%2F', '/')

            # if empty fields
            if url == "" and shortaux == "":
                httpCode = "404 not Found"
                htmlBody = "<html><body><p>Error,must fill both fields</p></body></html>"

                return httpCode, htmlBody

            # if URL doesn't start with http:// or https://
            if url[:7] != "http://" and url[:8] != "https://":
                url = "https://" + url
                short = '/' + shortaux
                self.content[short] = url

                httpCode = "200 OK"
                htmlBody = "<html><body>url: " + "<a href='" + url + \
                           "'>" + url + "</a><br>" + "Short: " + \
                           "<a href='" + self.content[short] + "'>" + \
                           short + "</a></body></html>"
                return httpCode, htmlBody
            else:
                short = '/' + shortaux
                # we add the url at the dictionary
                self.content[short] = url
                httpCode = "200 OK"
                htmlBody = "<html><body>url: " + "<a href='" + url + \
                           "'>" + url + "</a><br>" + "Short: " + \
                           "<a href='" + self.content[short] + "'>" + \
                           short + "</a></body></html>"
                return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = Shortener("localhost", 1234)
